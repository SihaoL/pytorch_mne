#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 14:27:54 2018

@author: sihao
"""
import numpy as np
from scipy.misc import imresize

def train_test_split(data, n, i):
    """Splits data into n segments and takes the ith segment as the test set
        Inputs:
            data: 2D matrix of temporal data with time T as columns
            n: how many segments to split data into
            i: which segment to select as test set (zero-index)
        Outputs:
            train_set: data without ith segment
            test_set: ith segment of data
    
    """
    if not i<n:
        raise ValueError('i must be smaller than n')
    # make 1D vector into 2d Matrix
    if len(data.shape)<2:
        data = np.reshape(data, (1, len(data)))
    
    T = data.shape[1]
    data = data[:,0:-np.mod(T,n)] # make data vector of length divisible by n
    T = data.shape[1]
    
        test_set = data[:,int(T/n)*i:int(T/n)*(i+1)]
    train_set = np.delete(data, np.s_[int(T/n)*i:int(T/n)*(i+1)], axis = 1)
    
    return train_set, test_set
  
def lag_stim(data, num_lag):    
    '''Introduce time lags of the stimulus as extra dimensions

        Inputs:
            data: 2D matrix of temporal data with time T as columns
            num_lag: number of time lags to include as extra dimensions
        Outputs:
            lagged_data: 2D matrix of time lagged data
    '''
    lagged_data = np.tile(data,[num_lag,1])
    
    for i,row in enumerate(lagged_data): #TODO: Roll in num_dim chunks
        lagged_data[i,:] = np.roll(row, -i)
    
    lagged_data = np.delete(lagged_data, np.s_[0:num_lag], axis = 1)
    return lagged_data

def compress_stim(data, time_compress, dim_compress):
    ''' Compresses number of time points and dimensions of stimulus.
        Assumes that first axis (rows) is time and second axis (columns) is dimensions
        
        Inputs:
            data: 2D matrix of temporal data with time T as columns
            time_compress: fraction to compress time axis by
            dim_compress: fraction to commpress dimension axis by
        Outputs:
            compressed_data: 2D matrix of compressed input data
    '''
    num_dim, num_time = data.shape
    output_time = int(np.ceil(num_time * time_compress))
    output_dim = int(np.ceil(num_dim * dim_compress))
    
    return imresize(data, (output_dim, output_time))



















