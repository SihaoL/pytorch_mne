#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 14:20:00 2018

@author: sihao
"""
# %% Imports
import torch

from torch.autograd import Variable
import torch.nn as nn

import seaborn
import matplotlib.pyplot as plt

import numpy as np
from fractions import Fraction

from data_utils import train_test_split

# %% Model definition
class MNE_Model(nn.Module):
    def __init__(self, D, stim, resp, train_frac=0.6):
        super(MNE_Model, self).__init__() #D is length of parameter vector
        self.model_params = nn.Parameter(0.001 * torch.randn(D), requires_grad=True)
        
        train_frac_num = Fraction(train_frac).limit_denominator().numerator;
        train_frac_den = Fraction(train_frac).limit_denominator().denominator;
        
        self.stim_train, self.stim_test = train_test_split(stim, train_frac_den, train_frac_num)
        self.stim_train = Variable(torch.from_numpy(self.stim_train.T)).type(torch.FloatTensor)
        self.stim_test = Variable(torch.from_numpy(self.stim_test.T)).type(torch.FloatTensor)
        
        self.resp_train, self.resp_test = train_test_split(resp, train_frac_den, train_frac_num)
        self.resp_train = Variable(torch.from_numpy(self.resp_train.T)).type(torch.FloatTensor).squeeze()
        self.resp_test = Variable(torch.from_numpy(self.resp_test)).type(torch.FloatTensor).squeeze()
        
#        self.stim_train = Variable(torch.from_numpy(train_test_split(stim,).T.type(torch.FloatTensor))
#        self.stim_test = Variable(torch.from_numpy(stim[:, int(np.ceil(0.6*stim.shape[1])):].T).type(torch.FloatTensor))

#        self.resp_train = Variable(torch.from_numpy(resp[:, 1:int(np.ceil(0.75*resp.shape[1]))]).type(torch.FloatTensor)).squeeze()
#        self.resp_test = Variable(torch.from_numpy(resp[:, int(np.ceil(0.75*resp.shape[1])):]).type(torch.FloatTensor)).squeeze()
        
        
        self.num_samples, self.num_dim = self.stim_train.size()


        
    def forward(self):
        
        a_ = self.extract_a() # a is reserved name for something else
        h_ = self.extract_h() # h is reserved name for something else
        J = self.extract_J()
        
        p_spike = self.predict_spike(a_, h_, J, self.stim_train)
#        self.p_spike = p_spike
        
        # %% Calculate model parameters from generated spike train
        
        _, _, _, model_params_new = self.calculate_model_params(self.stim_train, p_spike)

        return model_params_new
    
    def calculate_model_params(self, stim, p_spike):
        a_ = torch.mean(p_spike)
        h_ = torch.matmul(p_spike.squeeze(), stim).squeeze()
        h_ = h_/self.num_samples
        J = torch.matmul(torch.t(stim),(p_spike.expand(p_spike.size()[0], self.num_dim) * stim / self.num_samples))
        model_params = torch.cat((a_, h_.view(h_.numel()), J.view(J.numel())), 0)       
        
        return a_, h_, J, model_params
    
    def predict_spike(self, a_, h_, J, stim):
        p_spike = 1/(1+torch.exp(a_+torch.matmul(stim,h_.unsqueeze(1)).squeeze() + torch.sum(stim * torch.matmul(stim, J),1)))
        p_spike = p_spike.unsqueeze(1)
        
        return p_spike
    
    def targets(self):
        _, _, _, targets = self.calculate_model_params(self.stim_train, self.resp_train.unsqueeze(1))
        
        return targets
    
    def predict_spikes_with_current_params(self):
        a_ = self.model_params[0] # a is reserved name for something else
        h_ = self.model_params[1:self.num_dim+1] # h is reserved name for something else
        J = self.model_params[self.num_dim:self.num_dim + self.num_dim*self.num_dim]
        J = J.view(self.num_dim, self.num_dim)
        
        p_spike = self.predict_spike(a_, h_, J, self.stim_train)
        
        return p_spike
    
    def test(self):
        a_ = self.extract_a() # a is reserved name for something else
        h_ = self.extract_h() # h is reserved name for something else
        J = self.extract_J()
        
        p_spike = self.predict_spike(a_, h_, J, self.stim_test).squeeze()
        
        return p_spike
    
    def extract_a(self):
        a_ = self.model_params[0]#a is reserved for something else
        return a_
    
    def extract_h(self):
        h_ = self.model_params[1:self.num_dim+1] #h is reserved for something else
        return h_
    
    def extract_J(self):
        J = self.model_params[self.num_dim:self.num_dim + self.num_dim*self.num_dim]
        J = J.view(self.num_dim, self.num_dim)
        return J
    
    def plot_feature(self,n):
        J = self.extract_J()
        w, V = np.linalg.eig(J.data.numpy())
        w_sorted = np.argsort(w)
        
        feature = V[:,w_sorted[n]]
        feature = feature.reshape(int(np.sqrt(self.num_dim)),int(np.sqrt(self.num_dim)))
        
        fig, ax = plt.subplots()
        seaborn.heatmap(feature.real, cmap=plt.get_cmap('viridis'), ax=ax)
        
        return fig, ax
