#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 16:50:14 2017

@author: sihao
"""

# %% Imports
from MNE_Model import MNE_Model as Net

import torch.optim as optim
import torch.nn as nn
import matplotlib.pyplot as plt

import numpy as np


import scipy.io as io


# %% Load data

D= 65793

inputs = io.loadmat('/home/sihao/KozlovBox/APOLLO/Sihao/Data/MNE_input.mat')

stim_ = io.loadmat('/home/sihao/KozlovBox/APOLLO/Sihao/Data/MEng/Stimuli/USV_stim.mat')
# %% Initialise model
net = Net(D, inputs['stim_'], inputs['resp_'])

criterion = nn.BCEWithLogitsLoss()
optimiser = optim.Adam(net.parameters(), lr=1e-4)
net.zero_grad()

# %% Train model
for i in range(100):
    net.model_params
    optimiser.zero_grad()
    
    output = net.forward()
    loss= criterion(output, net.targets())
    loss.backward()
    optimiser.step()

#    if i % 10 == 9:    # every 10 iterations
#    plt.figure()
#    plt.plot(net.predict_spikes_with_current_params().data.numpy())
#    plt.show()


# %% Plotting
    
# Prediction and test response
fig1, ax1 = plt.subplots()
ax1.hold(True)
ax1.plot(net.test().data.numpy())
ax1.plot(net.resp_test.data.numpy())
# Correlation coefficient
corr_coeff =  np.corrcoef(net.test().data.numpy(), net.resp_test.data.numpy())
print(corr_coeff)
# Receptive field
fig2, ax2 = net.plot_feature(1)

